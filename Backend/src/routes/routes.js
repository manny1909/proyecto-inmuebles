const { Router } = require('express');
const router = Router();
const {catalogo,consultarMuebles,registrarCliente
        ,autenticar,estaAutenticado,agregar_al_carrito,
        crearVendedor, valor_total,realizarVenta
    ,clientes_nuevos,mejores_clientes,mejores_vendedores
    ,muebles_mas_vendidos} = require('../controllers/index')
router.route('/')
    .get(async(req,res)=>{
        const muebles=await catalogo();
        res.render('partials/catalogo',{muebles})
    })
    
router.route('/registrarse')
    .get((req,res)=>{
        res.render('partials/registrarCliente')
    })
    .post(async(req,res)=>{
        await registrarCliente(req)
        res.redirect('/')}
)

router.route('/autenticacion')
    .get()
    .post(autenticar)
router.route('/cerrarSesion')
.get(estaAutenticado,(req,res)=>{
    req.logOut()
    res.redirect('/')
})
router.route('/profile')
    .get(estaAutenticado,(req,res)=>{
        //console.log(req.user.id);
        res.render('partials/profile')
    })
router.route('/agregarAlCarrito')
    .get()
    .post(estaAutenticado,async(req,res)=>{
         await agregar_al_carrito(req)
        res.redirect('/')
    })
router.route('/carrito')
    .get(estaAutenticado,(req,res)=>{        
        var total=valor_total(req);
        const muebles_vendidos=req.session.carrito;
        req.session.valor_total=total;
        console.log(total);
        res.render('partials/carrito',{total,muebles_vendidos})
    })
    .post(estaAutenticado,async(req,res)=>{
        await realizarVenta(req)
        req.session.carrito=undefined;
        
        res.redirect('/')
    })
router.route('/crearVendedor')
    .get(estaAutenticado,(req,res)=>{
        res.render('partials/crearVendedor.hbs')
    })
    .post(estaAutenticado,async (req,res)=>{
      const e= await crearVendedor(req)
      res.redirect('/crearVendedor')
    })
router.route('/clientes-nuevos-mes')
    .get(estaAutenticado,async(req,res)=>{
        const resultado =await clientes_nuevos()
        res.render('partials/clientes_nuevos',{resultado})
    })
router.route('/mejores-clientes-mes')
    .get(estaAutenticado,async (req,res)=>{
        const resultado =await mejores_clientes()
        res.render('partials/mayores_compras_mes',{resultado})
    })

router.route('/mejores-vendedores-mes')
    .get(estaAutenticado,async(req,res)=>{
        const resultado=await mejores_vendedores()
        res.render('partials/mejores_vendedores_mes',{resultado})
    })
router.route('/muebles-mas-vendidos-mes')
    .get(estaAutenticado,async(req,res)=>{
        const resultado=await muebles_mas_vendidos()
        res.render('partials/muebles_mas_vendidos',{resultado})
    })
module.exports= router;