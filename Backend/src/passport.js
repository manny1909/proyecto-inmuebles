const passport=require('passport');
const LocalStrategy =require('passport-local').Strategy;
const Cliente=require('./models/cliente/cliente')
const Admin=require('./models/admin/admin')
const Vendedor=require('./models/vendedor/vendedor')
const con=require('./conexion')
passport.use(new LocalStrategy(
    {
        usernameField:'email',
        passwordField:'clave',

    },async(email,clave,done)=>{        
        var user={}
        const cliente=new Cliente({email,clave});
        const admin=new Admin({email,clave});
        const vendedor=new Vendedor({email,clave});        
        const ec=await cliente.autenticar();
        const ea=await admin.autenticar();
        const ev=await vendedor.autenticar();        
        if(!ec && !ea && !ev){            
            return done(null,false,{message:'datos incorrectos'})
        }
        else{
            if(ec){
                user={cliente,rol:'cliente'}; 
            }
            if(ea){
                user={admin,rol:'admin'}; 
            }
            if(ev){
                user={vendedor,rol:'vendedor'}; 
            }
            
            return done(null,user)
        }
    }
)
);
passport.serializeUser((user,done)=>{
    if(user.rol=='cliente'){done(null,user.cliente.id);}
    if(user.rol=='admin'){done(null,user.admin.id);}
    if(user.rol=='vendedor'){done(null,user.vendedor.id);}
    
})

passport.deserializeUser(async (id,done)=>{
    const cliente= new Cliente({id:id})
    const admin=new Admin({id:id})
    const vendedor=new Vendedor({id:id})
    cc =await cliente.consultar();
    ca =await admin.consultar();
    cv =await vendedor.consultar();
    if(!cc && !ca && !cv){
        done(null,false,{mensaje:'error'})
    }
    else{
        var user={}
        if(cc){
           user.id=cliente.id;
           user.nombre=cliente.nombre;
           user.apellidos=cliente.apellidos;
           user.email=cliente.email;
           user.rol='cliente'
        }
        if(ca){
            user.id=admin.id;
           user.nombre=admin.nombre;
           user.apellidos=admin.apellidos;
           user.email=admin.email;
           user.rol='admin'
        }
        if(cv){
            user.id=vendedor.id;
           user.nombre=vendedor.nombre;
           user.apellidos=vendedor.apellidos;
           user.email=vendedor.email;
           user.rol='vendedor'
        }     
        
            done(null,user)
        
        
    }
    
})

