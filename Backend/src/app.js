const express= require('express')
const app=express();
const cors=require('cors')
const morgan=require('morgan')
const handlebars=require('express-handlebars')
const session=require('express-session')
const flash=require('connect-flash')
const passport=require('passport')
require('./passport')
const path=require('path')


app.set('port', process.env.PORT || '3000')
app.set('views',path.join(__dirname,'vistas'))
app.engine('.hbs',handlebars(
    {
        defaultLayout:'main',
        layoutsDir:path.join(app.get('views'),'layouts'),
        partialsDir:path.join(app.get('views'),'partials'),
        extname:'.hbs',
        helpers: require('./helpers/handlebars')

    }
    
))
//middlewares
app.use('/api',express.static(path.join(__dirname,'publico')))
app.set('view engine','.hbs')
app.use(cors());
app.use(express.urlencoded({extended:false}))
app.use(express.json())
app.use(morgan('dev')) 
app.use(session({
    secret:'sdfsfdf',
    resave:false,
    saveUninitialized:false
}))
app.use(flash())
app.use(passport.initialize());
app.use(passport.session());
app.use((req,res,next)=>{
    res.locals.user=req.user || null 
    res.locals.errors=req.flash('errors')
    res.locals.msg_ok=req.flash('msg_ok')
    res.locals.error=req.flash('error')
 
    next();
})
//rutas
app.use('/api',require('./routes/routes'))


module.exports=app;

