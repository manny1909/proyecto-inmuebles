const Persona=require('../persona')
const vendedorDAO=require('../../persistence/vendedorDAO')
const conexion=require('../../conexion')

class Vendedor extends Persona{
    constructor(data){
        super(data);
        this.vendedorDAO=new vendedorDAO(data)
        this.con=conexion
    }
    async insertar(){
        const data=[this.id, this.nombre,this.apellidos,this.email,this.clave]
        const query=this.vendedorDAO.insertar();
        try{ await this.con.conexion.ejecutar(query,data)
            return this.con.conexion.estado;
        }
        catch(e){ 
            console.log(e);
        }
    }
    async autenticar(){
        try{
            const resultado= await  this.con.conexion.ejecutar(this.vendedorDAO.autenticar(),[this.email,this.clave])     
            
            if(resultado[0]){
                this.id=resultado[0].id
            this.nombre=resultado[0].nombre
            this.apellidos=resultado[0].apellidos
            this.email=resultado[0].email
                return true
            }
            else return false;
        }
        catch(e){console.log(e);}
    }
    async consultar(){
        try{
            const resultado= await  this.con.conexion.ejecutar(this.vendedorDAO.consultar(),[this.id])
            
            if(resultado[0]){
                this.id=resultado[0].id
            this.nombre=resultado[0].nombre
            this.apellidos=resultado[0].apellidos
            this.email=resultado[0].email
                return true
            }
            else return false;
        }
        catch(e){console.log(e);}
    }
}
module.exports=Vendedor;