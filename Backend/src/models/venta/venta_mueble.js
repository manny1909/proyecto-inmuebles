const conexion =require('../../conexion')
const Venta_muebleDAO=require('../../persistence/venta_muebleDAO')
class Venta_mueble{
    constructor(data){
        this.id_venta=data.id_venta;
        this.id_mueble=data.id_mueble;
        this.cantidad=data.cantidad;
        this.valor_total=data.valor_total;
        this.venta_muebleDAO=new Venta_muebleDAO(data)
        this.con=conexion
    }
    async insertar(){
        try{
            data=[this.id_venta,this.id_mueble,this.cantidad,this.valor_total]
        await this.con.conexion.ejecutar(this.venta_muebleDAO.insertar(),data);
        return this.con.conexion.estado;
        }
        catch(e){
            console.log(e);
        }
    }
}
module.exports=Venta_mueble;