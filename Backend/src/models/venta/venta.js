const { query } = require('express');
const conexion=require('../../conexion')
const VentaDAO=require('../../persistence/ventaDAO')
class venta{
    constructor(data){
        this.id=data.id;
        this.valor_total=data.valor_total;
        this.id_cliente=data.id_cliente;
        this.id_vendedor=data.id_vendedor;
        this.con=conexion
        this.ventaDAO=new VentaDAO(data)
    }
    async insertar(){
        try{
            data=[this.id_cliente,this.id_vendedor,this.valor_total]
            const resultado=await this.con.conexion.ejecutar(this.ventaDAO.insertar(),data)
            
        }
        catch(e){
            console.log(e);
        }
    }
    async getId(){
        const query="select max(id) from venta where id_cliente_fk=$1"
        const resultado=await this.con.conexion.ejecutar(query,[this.id_cliente])
        this.id=resultado[0].max
    }
}
module.exports=venta;