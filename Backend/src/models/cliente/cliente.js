const Persona=require('../persona')
const ClienteDAO=require('../../persistence/clienteDAO')
const conexion=require('../../conexion')
class Cliente extends Persona{
    constructor(data){
        super(data);
        this.clienteDAO=new ClienteDAO(data)
        this.con=conexion;
    }
    getId(){
        return this.id;
    }
    async insertar(){
        const data=[this.id, this.nombre,this.apellidos,this.email,this.clave]
        const query=this.clienteDAO.insertar();
        try{ await this.con.conexion.ejecutar(query,data)
            return this.con.conexion.estado;
        }
        catch(e){ 
            console.log(e);
        }
    }
    async autenticar(){
        try{
            const resultado= await  this.con.conexion.ejecutar(this.clienteDAO.autenticar(),[this.email,this.clave])
            
            if(resultado[0]){
                this.id=resultado[0].id
            this.nombre=resultado[0].nombre
            this.apellidos=resultado[0].apellidos
            this.email=resultado[0].email
                return true
            }
            else return false;
        }
        catch(e){console.log(e);}
    }
    async consultar(){
        try{
            const resultado= await  this.con.conexion.ejecutar(this.clienteDAO.consultar(),[this.id])
            
            if(resultado[0]){
                this.id=resultado[0].id
            this.nombre=resultado[0].nombre
            this.apellidos=resultado[0].apellidos
            this.email=resultado[0].email
                return true
            }
            else return false;
        }
        catch(e){console.log(e);}
    }
}
module.exports=Cliente;