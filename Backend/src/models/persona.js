class Persona{
    constructor(data){
        this.id= data.id;
        this.nombre=data.nombre;
        this.apellidos=data.apellidos;
        this.email=data.email;
        this.foto=data.foto;
        this.clave=data.clave;
    }
}
module.exports=Persona;