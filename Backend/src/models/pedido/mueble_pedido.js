class mueble_pedido{
    constructor({data}){
        this.id_pedido=data.id_pedido;
        this.id_mueble=data.id_mueble;
        this.cantidad=data.cantidad;
        this.valor_total=data.valor_total;
    }
}
module.exports=mueble_pedido;