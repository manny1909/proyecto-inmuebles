const Persona=require('../persona')
const AdminDAO=require('../../persistence/adminDAO')
const conexion=require('../../conexion')
class Admin extends Persona{
    constructor(data){
        super(data);
        this.adminDAO =new AdminDAO(data)
        this.con=conexion
    }
    async autenticar(){
        try{
            const resultado= await  this.con.conexion.ejecutar(this.adminDAO.autenticar(),[this.email,this.clave])
            
            if(resultado[0]){
                this.id=resultado[0].id
            this.nombre=resultado[0].nombre
            this.apellidos=resultado[0].apellidos
            this.email=resultado[0].email
            console.log( this.nombre,this.apellidos );
                return true
            }
            else {
                
                return false;}
        }
        catch(e){console.log(e);}
    }
    async consultar(){
        try{
            const resultado= await  this.con.conexion.ejecutar(this.adminDAO.consultar(),[this.id])
            
            if(resultado[0]){
                this.id=resultado[0].id
            this.nombre=resultado[0].nombre
            this.apellidos=resultado[0].apellidos
            this.email=resultado[0].email
                return true
            }
            else return false;
        }
        catch(e){console.log(e);}
    }
}
module.exports=Admin;