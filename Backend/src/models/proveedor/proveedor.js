class proveedor{
    constructor({data}){

        this.nit=data.nit;
        this.nombre=data.nombre;
        this.direccion=data.direccion;
        this.contacto=data.contacto;
        this.email=data.email;
        this.id_cat=data.id_cat;
    }
}
module.exports=proveedor;