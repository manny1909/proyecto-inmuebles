const conexion=require('../../conexion')
const Foto_muebleDAO=require('../../persistence/foto_muebleDAO')
const MuebleDAO=require('../../persistence/muebleDAO')
class mueble{
    constructor(data){
        this.id=data.id;
        this.nombre=data.nombre;
        this.id_material=data.id_material;
        this.largo=data.largo;
        this.ancho=data.ancho;
        this.alto=data.alto;
        this.id_color=data.id_color;
        this.precio=data.precio;
        this.nit=data.nit;
        this.cat=data.cat;
        this.con=conexion;
        this.muebleDAO=new MuebleDAO(data);
        this.foto_muebleDAO=new Foto_muebleDAO();
    }
    consultar=async()=>{
        const resultado= await this.con.conexion.ejecutar(this.muebleDAO.consultar(),[this.id])
        if(resultado){
            this.id=resultado[0].id
            this.nombre=resultado[0].nombre
            this.id_material=resultado[0].id_material_fk
            this.largo=resultado[0].largo
            this.ancho=resultado[0].ancho
            this.alto=resultado[0].alto
            this.id_color=resultado[0].id_color_fk
            this.precio=resultado[0].precio
            this.nit=resultado[0].nit_fk
            this.cat=resultado[0].id_cat_fk
        }
    }
    consultarTodos=async()=>{
        const muebles= new Array()
        var resultado= await this.con.conexion.ejecutar(this.muebleDAO.consultarTodos())
        
        for (let i = 0; i < resultado.length; i++) {
            const mueble={
                id: resultado[i].id,
            nombre: resultado[i].nombre,
            precio: resultado[i].precio,
            id_material: resultado[i].id_material_fk,
            id_color: resultado[i].id_color_fk,
            largo: resultado[i].largo,
            ancho: resultado[i].ancho,
            alto: resultado[i].alto,
            nit: resultado[i].nit_fk,
            cat: resultado[i].id_cat_fk,
            }
            
            
            muebles.push(mueble)            
        }
        return muebles;
    }
    async catalogo(){
        const muebles=await this.consultarTodos()
        for (let i = 0; i < muebles.length; i++) {
            const mueble = muebles[i]; 
            const fotos=await this.con.conexion.ejecutar(this.foto_muebleDAO.consultar(),[mueble.id])
            muebles[i].fotos=fotos
        }
        
        return muebles;
    }
}
module.exports=mueble;