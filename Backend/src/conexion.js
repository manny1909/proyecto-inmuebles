const { Pool } = require('pg');
const config={
  /* el archivo '.env' no se sube al repositorio pero en el se pone la configuracion de
  las variables globales como userDB que se refiere al usuario con el que entramos 
  a la base de datos (en este caso el usuario es postgres), 
  host que se refiere a la ip de la maquina que tiene la
  base de datos(en este caso ponemos la maquina local     es decir localhost)
  y por ultimo pass que es la contraseña del usuario(postgres) en este caso 1234*/
host: process.env.host||'localhost',
user: process.env.userDB||'postgres',
database: process.env.database||'prontomuebles',
password: process.env.password||'1234', 
port:5432,

}
const conexion = {
  pool: new Pool(config),
  estado: true,  
  
   ejecutar: async function ejecutar(query,data) {
     try {
      resultado =await this.pool.query(query,data)
      return (resultado)?resultado=resultado.rows:console.log('no hay resultado');
     } catch (error) {
       this.estado=false
       console.log(error);
     }
     
   }
  
}
module.exports = {conexion};