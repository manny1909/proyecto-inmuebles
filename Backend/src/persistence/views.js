
const conexion=require('../conexion')
const views={
    
    clientes_nuevos=async()=>{
        const query='select *from clientes_nuevos'
        resultado= await conexion.conexion.ejecutar(query)
        return resultado
    },
    clientes_mayores_compras_mes=async()=>{
        const query='select *from clientes_compras_ventas_mes'
        resultado= await conexion.conexion.ejecutar(query)
        return resultado
    },
    top_vendedores_mes=async()=>{
        const query='select *from top_vendedores_mes'
        resultado= await conexion.conexion.ejecutar(query)
        return resultado
    },
    muebles_mas_vendidos_mes=async()=>{
        const query='select *from muebles_mas_vendidos_mes'
        resultado= await conexion.conexion.ejecutar(query)
        return resultado
    }


    
}
module.exports=views;