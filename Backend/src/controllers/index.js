const indexCrl = {};
const con=require('../conexion')
const Cliente = require('../models/cliente/cliente')
const Mueble = require('../models/mueble/mueble')
const Admin = require('../models/admin/admin')
const Vendedor = require('../models/vendedor/vendedor')
const Venta =require('../models/venta/venta')
const Venta_mueble =require('../models/venta/venta_mueble')
const passport = require('passport');
const Venta_muebleDAO = require('../persistence/venta_muebleDAO');
indexCrl.estaAutenticado = (req, res, next) => {
    if (req.isAuthenticated()) {
        return next()
    }
    else {
        req.flash('errors', 'no ha iniciado session')
        res.redirect('/')
    }
}
indexCrl.catalogo = async (req, res) => {
    mueble = new Mueble({})
    const muebles = await mueble.catalogo();
    return muebles;
}
indexCrl.consultarMuebles = async (req, res) => {
    mueble = new Mueble({})
    const muebles = await mueble.consultarTodos();
    return muebles
}
indexCrl.registrarCliente = async (req, res) => {
    const { id, nombre, apellidos, email, clave } = req.body

    data = { id, nombre, apellidos, email, clave }
    const cliente = new Cliente(data)
    try {
        resultado = await cliente.insertar()
        req.flash('msg_ok','cliente registrado')
        return resultado

    } catch (e) {
        req.flash('errors','error al registrarse')
        console.log(e );
        return false
    }
}
indexCrl.autenticar = passport.authenticate('local', {
    failureRedirect: '/',
    successRedirect: '/profile',
    failureFlash: true
});
indexCrl.agregar_al_carrito = async (req, res) => {
    var { idmueble, cantidad, precio } = req.body
    idmueble=parseInt(idmueble);
    cantidad=parseInt(cantidad)
    if(req.session.carrito==undefined){
        req.session.carrito=[]
    }
    const carrito=req.session.carrito
    
    const pos = verificar(carrito, idmueble)
    if (cantidad > 0) {
        if (pos == -1) {
            const valor_total = cantidad * precio;
            mueble={  idmueble, cantidad, valor_total }
            req.session.carrito.push(mueble)
            req.flash('msg_ok', 'item agregado')
        }
        else {
            carrito[pos].cantidad += cantidad;
            carrito[pos].valor_total= precio * carrito[pos].cantidad
            req.flash('msg_ok', 'item actualizado')
        }
    }
    else {
        req.flash('errors', 'cantidad nula')
    }
 
}
indexCrl.realizarVenta=async(req,res)=>{
    if(req.user.rol=='cliente'){
        data={id_cliente:req.user.id,id_vendedor:1,valor_total:req.session.valor_total}
        var venta=new Venta(data)
        const carrito=req.session.carrito
        await venta.insertar()
        await venta.getId()
        console.log(venta);
        for (let i = 0; i < carrito.length; i++) {
            data={id_venta:venta.id,id_mueble:carrito[i].idmueble,cantidad:carrito[i].cantidad,valor_total:carrito[i].valor_total}
            const venta_mueble=new Venta_mueble(data)
            await venta_mueble.insertar()
            
        }
        req.flash('msg_ok','compra realizada con exito')
    }
}
indexCrl.valor_total=(req,res)=>{
    var total=0;
    if(req.session.carrito!=undefined){
        var carrito=req.session.carrito    
    for (let i = 0; i < carrito.length; i++) {
        total=parseInt(total)+ parseInt(carrito[i].valor_total);
        
    }
    }
    return total;
}
indexCrl.crearVendedor=async(req,res)=>{
    const {id,nombre,apellidos,email,clave}=req.body
    const vendedor=new Vendedor({id,nombre,apellidos,email,clave})
    console.log(vendedor);
    try {
        await vendedor.insertar()
        req.flash('msg_ok','vendedor registrado')
    } catch (error) {
        console.log(error);
        req.flash('errors',' vendedor no registrado')
    }
    
}
indexCrl.clientes_nuevos=async(req,res)=>{
    const query='select *from clientes_nuevos'
    resultado=con.conexion.ejecutar(query) ||null
    return resultado
}
indexCrl.mejores_clientes=async(req,res)=>{
    const query='select *from clientes_mayores_compras_mes'
    resultado=con.conexion.ejecutar(query) ||null
    return resultado
}
indexCrl.mejores_vendedores=async (req,res)=>{
    const query='select *from top_vendedores_mes'
    resultado=con.conexion.ejecutar(query) ||null
    return resultado
}
indexCrl.muebles_mas_vendidos=async (req,res)=>{
    const query='select *from muebles_mas_vendidos_mes'
    resultado=con.conexion.ejecutar(query) ||null
    return resultado
}
function verificar(lista,id){
    var pos = -1;
    for (let i = 0; i < lista.length; i++) {
        if (lista[i].idmueble ==id) {
            pos = i;            
            break;
        }
    }
    //console.log(lista);
    return pos;
}
module.exports = indexCrl;