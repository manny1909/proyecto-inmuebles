require('dotenv').config();
const app=require('./app')
const {conexion}=require('./conexion')
async function main(){
    await app.listen(app.get('port'))
    console.log('server on port ', app.get('port'));
    console.log('database: ', conexion.pool.options);
}
main();