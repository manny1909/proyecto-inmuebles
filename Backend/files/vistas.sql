create or replace view clientes_nuevos as
select distinct c.id,c.nombre,c.apellidos,c.email from cliente as c
inner join venta as v
on v.id_cliente_fk=c.id
where date_part('month',v.fecha)=date_part('month',current_date)
and date_part('year',v.fecha)=date_part('year',current_date) and c.id not in(
select v.id_cliente_fk from venta as v
where date_part('month',v.fecha)<date_part('month',current_date)
or date_part('year',v.fecha)<date_part('year',current_date));

create or replace view clientes_mayores_compras_mes as
select c.id,c.nombre,c.apellidos,c.email,sum(v.valor_total) as total from cliente as c
inner join venta as v
on v.id_cliente_fk=c.id
where date_part('month',v.fecha)=date_part('month',current_date)
and date_part('year',v.fecha)=date_part('year',current_date)
group by c.id order by total desc limit 10;


create or replace view top_vendedores_mes as
select e.id,e.nombre,e.apellidos,e.email,count(v.id_vendedor_fk) as ventas from vendedor as e
inner join venta as v
on v.id_vendedor_fk=e.id
where date_part('month',v.fecha)=date_part('month',current_date)
and date_part('year',v.fecha)=date_part('year',current_date)
group by e.id order by ventas desc limit 10;

create or replace view muebles_mas_vendidos_mes as
select m.id,m.nombre,count(vm.id_mueble_fk) as cantidad from mueble as m
inner join venta_mueble as vm
on vm.id_mueble_fk=m.id
inner join venta as v
on vm.id_venta_fk=v.id
where date_part('month',v.fecha)=date_part('month',current_date)
and date_part('year',v.fecha)=date_part('year',current_date) 
group by m.id order by cantidad desc limit 10;