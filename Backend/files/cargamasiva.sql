copy admin(id,nombre,apellidos,email) from '/home/manny/Escritorio/csv/admin.csv'
WITH (FORMAT csv, HEADER, DELIMITER ';', QUOTE '"');
copy tel_admin(telefono,id_admin_fk) from '/home/manny/Escritorio/csv/tel_admin.csv'
WITH (FORMAT csv, HEADER, DELIMITER ';', QUOTE '"');
copy cliente(id,nombre,apellidos,email) from '/home/manny/Escritorio/csv/cliente.csv'
WITH (FORMAT csv, HEADER, DELIMITER ';', QUOTE '"');
copy tel_cliente(telefono,id_cliente_fk) from '/home/manny/Escritorio/csv/tel_cliente.csv'
WITH (FORMAT csv, HEADER, DELIMITER ';', QUOTE '"');
copy dir_cliente(direccion,id_cliente_fk) from '/home/manny/Escritorio/csv/dir_cliente.csv'
WITH (FORMAT csv, HEADER, DELIMITER ';', QUOTE '"');
copy vendedor(id,nombre,apellidos,email) from '/home/manny/Escritorio/csv/vendedor.csv'
WITH (FORMAT csv, HEADER, DELIMITER ';', QUOTE '"');
copy tel_vendedor(telefono,id_vendedor_fk) from '/home/manny/Escritorio/csv/tel_vendedor.csv'
WITH (FORMAT csv, HEADER, DELIMITER ';', QUOTE '"');
copy inventario(fecha_creacion,fecha_modificacion,observaciones) from '/home/manny/Escritorio/csv/inventario.csv'
WITH (FORMAT csv, HEADER, DELIMITER ';', QUOTE '"');
copy categoria(descripcion) from '/home/manny/Escritorio/csv/categoria.csv'
WITH (FORMAT csv, HEADER, DELIMITER ';', QUOTE '"');
copy proveedor(nit,nombre,direccion,contacto,email,id_cat_fk) from '/home/manny/Escritorio/csv/proveedor.csv'
WITH (FORMAT csv, HEADER, DELIMITER ';', QUOTE '"');
copy tel_prov(telefono,nit_fk) from '/home/manny/Escritorio/csv/tel_prov.csv'
WITH (FORMAT csv, HEADER, DELIMITER ';', QUOTE '"');
copy pedido(valor_total,fecha,estado,nit_prov_fk,id_admin_fk) from '/home/manny/Escritorio/csv/pedido.csv'
WITH (FORMAT csv, HEADER, DELIMITER ';', QUOTE '"');
copy venta(valor_total,id_cliente_fk,id_vendedor_fk) from '/home/manny/Escritorio/csv/venta.csv'
WITH (FORMAT csv, HEADER, DELIMITER ';', QUOTE '"');
copy color(color) from '/home/manny/Escritorio/csv/color.csv'
WITH (FORMAT csv, HEADER, DELIMITER ';', QUOTE '"');
copy material(material) from '/home/manny/Escritorio/csv/material.csv'
WITH (FORMAT csv, HEADER, DELIMITER ';', QUOTE '"');
copy mueble(id_material_fk,largo,ancho,alto,id_color_fk,precio,nit_fk,id_cat_fk) from '/home/manny/Escritorio/csv/mueble.csv'
WITH (FORMAT csv, HEADER, DELIMITER ';', QUOTE '"');
copy mueble_pedido(id_pedido_fk,id_mueble_fk,cantidad,valor_total) from '/home/manny/Escritorio/csv/mueble_pedido.csv'
WITH (FORMAT csv, HEADER, DELIMITER ';', QUOTE '"');
copy venta_mueble(id_venta_fk,id_mueble_fk,cantidad,valor_total) from '/home/manny/Escritorio/csv/venta_mueble.csv'
WITH (FORMAT csv, HEADER, DELIMITER ';', QUOTE '"');
copy inventario_mueble(id_inventario_fk,id_mueble_fk,cantidad) from '/home/manny/Escritorio/csv/inventario_mueble.csv'
WITH (FORMAT csv, HEADER, DELIMITER ';', QUOTE '"');
copy foto_mueble(id_mueble_fk,foto) from '/home/manny/Escritorio/csv/foto_mueble.csv'
WITH (FORMAT csv, HEADER, DELIMITER ';', QUOTE '"');

