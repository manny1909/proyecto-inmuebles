create database prontomuebles;
\c prontomuebles;
create table if not exists public.admin(
  id numeric not null,
  nombre text not null,
  apellidos text not null,
  email text not null,
  primary key(id)  
);
create table if not exists public.tel_admin(
    telefono numeric not null,
    id_admin_fk integer not null,
    primary key(telefono, id_admin_fk),
    foreign key (id_admin_fk) references admin(id)
);
create table if not exists public.cliente(
  id numeric not null,
  nombre text not null,
  apellidos text not null,
  email text not null,
  primary key(id)  
);
create table if not exists public.tel_cliente(
    telefono numeric not null,
    id_cliente_fk integer not null,
    primary key(telefono, id_cliente_fk),
    foreign key (id_cliente_fk) references cliente(id)
);
create table if not exists public.dir_cliente(
    direccion text not null,
    id_cliente_fk integer not null,
    primary key(direccion, id_cliente_fk),
    foreign key (id_cliente_fk) references cliente(id)
);
create table if not exists public.vendedor(
    id numeric not null,
  nombre text not null,
  apellidos text not null,
  email text not null,
  primary key(id)  
);
create table if not exists public.tel_vendedor(
    telefono numeric not null,
    id_vendedor_fk integer not null,
    primary key(telefono, id_vendedor_fk),
    foreign key (id_vendedor_fk) references vendedor(id)
);
create table if not exists public.inventario(
    id serial not null,
    fecha_creacion date not null,
    fecha_modificacion date not null,
    observaciones text,
    primary key(id)
);
create table if not exists public.categoria(
    id serial not null,
    descripcion text not null,
    primary key(id)
);
create table if not exists public.proveedor(
    nit numeric not null,
    nombre text not null,
    direccion text,
    contacto text not null,
    email text not null,
    id_cat_fk integer not null,
    primary key(nit),
    foreign key(id_cat_fk) references categoria(id)
);
create table if not exists public.tel_prov(
    telefono text not null,
    nit_fk integer not null,
    primary key(telefono,nit_fk),
    foreign key(nit_fk) references proveedor(nit)
);
create table if not exists public.pedido(
    id serial not null,
    valor_total numeric not null,
    fecha date not null,
    estado text not null,
    nit_prov_fk integer not null,
    id_admin_fk integer not null,
    primary key(id),
    foreign key(nit_prov_fk) references proveedor(nit), 
    foreign key(id_admin_fk) references admin(id)
);
create table if not exists public.venta(
    id serial not null,
    valor_total numeric not null,
    id_cliente_fk integer not null,
    id_vendedor_fk integer not null,
    primary key(id),
    foreign key(id_cliente_fk) references cliente(id),
    foreign key(id_vendedor_fk) references vendedor(id)    
);
create table if not exists public.color(
    id_color serial not null,
    color text not null,
    primary key(id_color)
);
create table if not exists public.material(
    id_material serial not null,
    material text not null,
    primary key(id_material)
);
create table if not exists public.mueble(
    id serial not null,
    nombre text not null default 'sin nombre',
    id_material_fk integer not null,
    largo decimal not null,
    ancho decimal not null,
    alto decimal not null,
    id_color_fk integer not null,
    precio numeric not null,
    nit_fk integer not null,
    id_cat_fk integer not null,
    primary key(id),
    foreign key(id_material_fk) references material(id_material),
    foreign key(id_color_fk) references color(id_color),
    foreign key(nit_fk) references proveedor(nit),
    foreign key(id_cat_fk) references categoria(id)
);
create table if not exists public.mueble_pedido(
    id_pedido_fk integer not null,
    id_mueble_fk integer not null,
    cantidad numeric not null,
    valor_total numeric not null,
    primary key(id_pedido_fk,id_mueble_fk),
    foreign key(id_pedido_fk) references pedido(id),
    foreign key(id_mueble_fk) references mueble(id)
);
create table if not exists public.venta_mueble(
    id_venta_fk integer not null,
    id_mueble_fk integer not null,
    cantidad numeric not null,
    valor_total numeric not null,
    primary key(id_venta_fk,id_mueble_fk),
    foreign key(id_venta_fk) references venta(id),
    foreign key(id_mueble_fk) references mueble(id)
);
create table if not exists public.inventario_mueble(
    id_inventario_fk integer not null,
    id_mueble_fk integer not null,
    cantidad numeric,
    primary key(id_inventario_fk, id_mueble_fk),
    foreign key(id_inventario_fk) references inventario(id),
    foreign key(id_mueble_fk) references mueble(id)
);
create table if not exists public.foto_mueble(
    foto text not null,
    id_mueble_fk integer not null,
    primary key(foto,id_mueble_fk),
    foreign key(id_mueble_fk) references public.mueble(id)
);
create table if not exists public.reporte(
    id serial not null,
    nombre text not null,
    primary key(id)
);
alter table public.admin add column if not exists foto text;
alter table public.admin add column if not exists clave text not null default md5('0');
alter table public.cliente add column if not exists foto text;
alter table public.cliente add column if not exists clave text not null default md5('0');
alter table public.vendedor add column if not exists foto text;
alter table public.vendedor add column if not exists clave text not null default md5('0');
alter table public.categoria add column if not exists foto text;
alter table public.venta add column if not exists valor_instalacion numeric not null default 0;
alter table public.venta add column if not exists fecha date not null default current_date;
